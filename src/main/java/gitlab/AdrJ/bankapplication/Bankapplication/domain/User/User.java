package gitlab.AdrJ.bankapplication.Bankapplication.domain.User;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.Account;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Auditable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@Setter
@Builder
@Table(name = "USERS")
public class User extends Auditable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "user_sequence")
    @SequenceGenerator(name = "user_sequence")
    long id;

    @Enumerated(EnumType.STRING)
    Gender gender;

    String firstName;
    String lastName;

    @Column(unique = true)
    String login;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Account> accounts = new HashSet<>();

    static User generateUser(UserDto userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setLogin(userDto.getLogin());
        user.setGender(userDto.getGender());
        return user;
    }

    public void addAccount(Account account) {
        accounts.add(account);
        account.setUser(this);
    }
}
