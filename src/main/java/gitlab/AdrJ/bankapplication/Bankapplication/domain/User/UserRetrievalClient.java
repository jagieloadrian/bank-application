package gitlab.AdrJ.bankapplication.Bankapplication.domain.User;

public interface UserRetrievalClient {

    User getById(long userId);
}
