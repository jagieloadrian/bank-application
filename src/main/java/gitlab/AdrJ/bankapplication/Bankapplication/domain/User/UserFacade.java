package gitlab.AdrJ.bankapplication.Bankapplication.domain.User;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserFacade {

    private final CreateUser createUser;
    private final UpdateUserClient updateUserClient;

    public void create(UserDto userDto) {
        createUser.createUser(userDto);
    }

    public void changeLogin(long userId, String newLogin) {
        updateUserClient.changeLogin(userId, newLogin);
    }
}
