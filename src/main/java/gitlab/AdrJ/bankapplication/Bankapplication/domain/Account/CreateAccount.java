package gitlab.AdrJ.bankapplication.Bankapplication.domain.Account;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.User;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.UserRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class CreateAccount {

    private final UserRetrievalClient userRetrievalClient;

    public void createAccount(long userId) {
        User user = userRetrievalClient.getById(userId);
        user.addAccount(Account.createAccount());
    }


}
