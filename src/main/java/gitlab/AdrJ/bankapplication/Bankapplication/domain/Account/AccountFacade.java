package gitlab.AdrJ.bankapplication.Bankapplication.domain.Account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountFacade {

    private final CreateAccount createAccount;

    public void createAccount(long userId) {
        createAccount.createAccount(userId);
    }
}
