package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.Account;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
class ValidateAccount {

    public void validateAmount(Account accountFrom, BigDecimal amount) {
        if (accountFrom.getAmount().compareTo(amount) < 0) {
            TransferException.notEnoughMoney();
        }
    }

}
