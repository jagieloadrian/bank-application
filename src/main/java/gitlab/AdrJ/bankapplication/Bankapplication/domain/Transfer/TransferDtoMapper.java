package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

import gitlab.AdrJ.bankapplication.Bankapplication.api.transfer.TransferRequest;

public class TransferDtoMapper {

    public static TransferDto transferDto(TransferRequest transferRequest) {
        return TransferDto.builder()
                .ownerId(transferRequest.getOwnerId())
                .ownerAccountId(transferRequest.getOwnerAccountId())
                .clientAccountNumber(transferRequest.getClientAccountNumber())
                .amount(transferRequest.getTotalPrice())
                .title(transferRequest.getTitle())
                .build();
    }
}
