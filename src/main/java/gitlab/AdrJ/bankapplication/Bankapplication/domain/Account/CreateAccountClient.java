package gitlab.AdrJ.bankapplication.Bankapplication.domain.Account;

public interface CreateAccountClient {
    void create(Account account);
}
