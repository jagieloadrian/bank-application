package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.Account;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.AccountException;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.AccountRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
class CreateTransfer {

    private final AccountRetrievalClient accountRetrievalClient;
    private final CreateTransferClient createTransferClient;
    private final MakeTransfer makeTransfer;
    private final TransferRetrievalClient transferRetrievalClient;

    public void createTransfer(TransferDto transferDto) {
        String accountNumberTo = getAccountNumberByIdOrThrow(transferDto);
        Transfer transfer = Transfer.create(transferDto, accountNumberTo);
        makeTransfer.makeTransfer(transfer);
        createTransferClient.create(transfer);
    }

    void payOutstandingTransfers() {
        List<TransferDto> transfers = transferRetrievalClient.getPendingOrdersToPaid();
        for (TransferDto transferDto : transfers) {
            createTransfer(transferDto);
        }
    }

    private String getAccountNumberByIdOrThrow(TransferDto transferDto) {
        Optional<Account> number = accountRetrievalClient.findByOwnerAccountIdAndOwnerId(transferDto);
        Account account = number.orElseThrow(() -> AccountException.accountByIdAndUserIdNotFound(transferDto));
        return account.getAccountNumber();
    }

}
