package gitlab.AdrJ.bankapplication.Bankapplication.domain.Account;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Auditable;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.User;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Account extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "account_sequence")
    @SequenceGenerator(name = "account_sequence")
    @Setter(value = AccessLevel.PACKAGE)
    private long id;
    private String accountNumber;
    private BigDecimal amount;
    private BigDecimal fee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public static Account createAccount() {
        Account account = new Account();
        account.accountNumber = generateAccountNumber();
        account.amount = BigDecimal.TEN;
        account.fee = BigDecimal.TEN;
        return account;
    }

    private static String generateAccountNumber() {
        String uuidWithoutSpecialCharacters = UUID.randomUUID().toString().replaceAll("[-|a-z]", "0");
        return uuidWithoutSpecialCharacters.substring(0, 26);
    }

    public void subtractAmount(BigDecimal amount) {
        this.amount = this.amount.subtract(amount);
    }

    public void addAmount(BigDecimal amount) {
        this.amount = this.amount.add(amount);
    }
}
