package gitlab.AdrJ.bankapplication.Bankapplication.domain.User;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
public class UserDto {

    @NonNull
    private final String firstName;
    @NonNull
    private final String lastName;
    @NonNull
    private final String login;

    private final Gender gender;
}
