package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

public class TransferException extends IllegalArgumentException {

    TransferException(String message) {
        super(message);
    }

    public static void notEnoughMoney() {
        String message = String.format("Not enough money to paid");
        throw new TransferException(message);
    }
}
