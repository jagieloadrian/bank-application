package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;

@Builder
@Getter
public class TransferDto {
    @NonNull
    private final long ownerId;
    @NonNull
    private final long ownerAccountId;
    @NonNull
    private final String clientAccountNumber;
    @NonNull
    private final BigDecimal amount;
    @NonNull
    private final String title;
}
