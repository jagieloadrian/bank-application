package gitlab.AdrJ.bankapplication.Bankapplication.domain.User;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
class CreateUser {

    private final CreateUserClient createUserClient;

    public void createUser(UserDto userDto) {
        User user = User.generateUser(userDto);
        createUserClient.create(user);
    }
}
