package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class TransferFacade {

    private final CreateTransfer createTransfer;

    public void create(TransferDto transferDto) {
        createTransfer.createTransfer(transferDto);
    }

    public void payOutstandingTransfers() {
        createTransfer.payOutstandingTransfers();
    }
}
