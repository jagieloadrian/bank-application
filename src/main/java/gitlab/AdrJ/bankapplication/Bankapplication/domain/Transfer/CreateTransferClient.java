package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

public interface CreateTransferClient {
    void create(Transfer transfer);
}
