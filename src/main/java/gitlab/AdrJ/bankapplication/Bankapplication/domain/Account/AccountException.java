package gitlab.AdrJ.bankapplication.Bankapplication.domain.Account;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferDto;

public class AccountException extends IllegalArgumentException {
    AccountException(String message) {
        super(message);
    }

    public static AccountException accountByIdAndUserIdNotFound(TransferDto transferDto) {
        String message = String.format("Account by id %d not found", transferDto.getOwnerAccountId());
        throw new AccountException(message);
    }
}
