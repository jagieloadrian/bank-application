package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Setter
@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Table(name = "Transfer")
public class Transfer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "transfer_sequence")
    @SequenceGenerator(name = "transfer_sequence")
    @Setter(value = AccessLevel.PRIVATE)
    private Long id;
    @NonNull
    private String accountNumberFrom;
    @NonNull
    private String accountNumberTo;

    private BigDecimal amount;
    @NonNull
    private String title;

    public static Transfer create(TransferDto transferDto, String accountNumberTo) {
        Transfer transfer = new Transfer();
        transfer.setAccountNumberFrom(transferDto.getClientAccountNumber());
        transfer.setAccountNumberTo(accountNumberTo);
        transfer.setAmount(transferDto.getAmount());
        transfer.setTitle(transferDto.getTitle());
        return transfer;
    }
}
