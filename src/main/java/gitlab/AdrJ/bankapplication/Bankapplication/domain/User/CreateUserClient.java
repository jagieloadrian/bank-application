package gitlab.AdrJ.bankapplication.Bankapplication.domain.User;

public interface CreateUserClient {

    void create(User user);
}
