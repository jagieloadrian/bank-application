package gitlab.AdrJ.bankapplication.Bankapplication.domain.Account;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferDto;

import java.util.Optional;

public interface AccountRetrievalClient {
    Optional<Account> findByOwnerAccountIdAndOwnerId(TransferDto transferDto);

    Account findByAccountNumber(String accountNumber);
}
