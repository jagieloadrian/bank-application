package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.Account;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.AccountRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
class MakeTransfer {

    private final AccountRetrievalClient accountRetrievalClient;
    private final ValidateAccount validateAccount;

    @Transactional
    public void makeTransfer(Transfer transfer) {
        Account accountTo = findByAccountNumber(transfer.getAccountNumberTo());
        Account accountFrom = findByAccountNumber(transfer.getAccountNumberFrom());
        validateAccount.validateAmount(accountFrom, transfer.getAmount());
        accountFrom.subtractAmount(transfer.getAmount());
        accountTo.addAmount(transfer.getAmount());
    }

    private Account findByAccountNumber(String accountNumber) {
        Account account = accountRetrievalClient.findByAccountNumber(accountNumber);
        return account;
    }
}
