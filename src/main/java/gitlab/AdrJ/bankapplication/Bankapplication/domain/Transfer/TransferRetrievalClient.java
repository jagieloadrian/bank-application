package gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer;

import java.util.List;

public interface TransferRetrievalClient {

    List<TransferDto> getPendingOrdersToPaid();
}
