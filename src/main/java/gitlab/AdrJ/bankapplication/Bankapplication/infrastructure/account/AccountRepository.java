package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.account;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByIdAndUserId(long accountId, long userId);

    Account findByAccountNumber(String accountNumber);
}
