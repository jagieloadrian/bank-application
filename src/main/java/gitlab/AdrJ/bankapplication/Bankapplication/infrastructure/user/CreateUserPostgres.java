package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.user;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.CreateUserClient;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class CreateUserPostgres implements CreateUserClient {

    private final UserRepository userRepository;


    @Override
    public void create(User user) {
        userRepository.save(user);
    }
}
