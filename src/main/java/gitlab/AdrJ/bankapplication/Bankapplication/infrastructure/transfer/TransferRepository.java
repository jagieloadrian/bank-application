package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.transfer;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransferRepository extends JpaRepository<Transfer, Long> {
}
