package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.user;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
