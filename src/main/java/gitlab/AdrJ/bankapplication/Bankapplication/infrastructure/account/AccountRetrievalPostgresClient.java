package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.account;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.Account;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.AccountRetrievalClient;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
class AccountRetrievalPostgresClient implements AccountRetrievalClient {

    private final AccountRepository accountRepository;

    @Override
    public Optional<Account> findByOwnerAccountIdAndOwnerId(TransferDto transferDto) {
        return accountRepository.findByIdAndUserId(transferDto.getOwnerAccountId(), transferDto.getOwnerId());
    }

    @Override
    public Account findByAccountNumber(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }
}
