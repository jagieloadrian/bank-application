package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.transfer;

import gitlab.AdrJ.bankapplication.Bankapplication.api.transfer.TransferRequest;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferDto;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferDtoMapper;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferRetrievalClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
class TransferRetrievalRestClient implements TransferRetrievalClient {

    private RestTemplate restTemplate;
    private String pendingTransferUrl;

    @Autowired
    public TransferRetrievalRestClient(RestTemplate restTemplate,
                                       @Value("${pending.transfers.retrieval.url}") String pendingTransfersUrl) {
        this.restTemplate = restTemplate;
        this.pendingTransferUrl = pendingTransfersUrl;
    }


    @Override
    public List<TransferDto> getPendingOrdersToPaid() {
        List<TransferRequest> transferRequests = restTemplate.exchange(
                pendingTransferUrl,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<TransferRequest>>() {
                }).getBody();
        return transferRequests.stream()
                .map(TransferDtoMapper::transferDto)
                .collect(Collectors.toList());

    }
}
