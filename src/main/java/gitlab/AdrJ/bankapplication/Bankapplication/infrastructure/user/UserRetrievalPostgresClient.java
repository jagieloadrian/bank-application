package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.user;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.User;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.UserRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class UserRetrievalPostgresClient implements UserRetrievalClient {

    private final UserRepository userRepository;

    @Override
    public User getById(long userId) {
        User user = userRepository.getOne(userId);
        return user;
    }
}
