package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.user;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.UpdateUserClient;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class UpdateUserPostgresClient implements UpdateUserClient {

    private final UserRepository userRepository;

    @Override
    public void changeLogin(long userId, String login) {
        User user = userRepository.getOne(userId);
        user.setLogin(login);
    }
}
