package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.account;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.Account;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.CreateAccountClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class CreateAccountPostgresClient implements CreateAccountClient {

    private final AccountRepository accountRepository;

    @Override
    public void create(Account account) {
        accountRepository.save(account);
    }
}
