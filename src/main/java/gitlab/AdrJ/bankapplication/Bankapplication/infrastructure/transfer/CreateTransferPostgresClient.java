package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.transfer;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.CreateTransferClient;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.Transfer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class CreateTransferPostgresClient implements CreateTransferClient {

    private final TransferRepository transferRepository;

    @Override
    public void create(Transfer transfer) {
        transferRepository.save(transfer);
    }
}
