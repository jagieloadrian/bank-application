package gitlab.AdrJ.bankapplication.Bankapplication.api.transfer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Value
@Builder
@JsonDeserialize(builder = TransferRequest.TransferRequestBuilder.class)
public class TransferRequest {


    long ownerId;

    @Min(1)
    long ownerAccountId;

    @NotEmpty
    @NonNull String clientAccountNumber;

    @NonNull
    BigDecimal totalPrice;

    @NonNull
    @NotEmpty
    String title;

    @JsonPOJOBuilder(withPrefix = "")
    public static class TransferRequestBuilder {
    }
}
