package gitlab.AdrJ.bankapplication.Bankapplication.api.user;


import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.Gender;
import lombok.Data;
import lombok.Value;

import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Value
class UserRequest {

    @NotNull
    @NotEmpty
    private final String firstName;

    @NotNull
    @NotEmpty
    private final String lastName;

    @NotNull
    @NotEmpty
    private final String login;

    @Enumerated
    private final Gender gender;

    @Data
    static class LoginRequest {

        @NotEmpty
        @Size(min = 6)
        String login;
    }
}
