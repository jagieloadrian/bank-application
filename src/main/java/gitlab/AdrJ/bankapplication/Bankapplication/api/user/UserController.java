package gitlab.AdrJ.bankapplication.Bankapplication.api.user;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.UserDto;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.UserFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
class UserController {

    private final UserFacade userFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createUser(@Valid @RequestBody UserRequest userRequest) {
        UserDto userDto = userDto(userRequest);
        userFacade.create(userDto);
    }

    @PutMapping(path = "/{userId}/change-login")
    @ResponseStatus(HttpStatus.OK)
    public void updateLogin(@Min(1) @PathVariable long userId,
                            @RequestBody UserRequest.LoginRequest loginRequest) {
        userFacade.changeLogin(userId, loginRequest.getLogin());
    }

    private UserDto userDto(UserRequest userRequest) {
        UserDto userDto = UserDto.builder()
                .firstName(userRequest.getFirstName())
                .lastName(userRequest.getLastName())
                .login(userRequest.getLogin())
                .gender(userRequest.getGender())
                .build();
        return userDto;
    }
}
