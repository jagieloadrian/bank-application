package gitlab.AdrJ.bankapplication.Bankapplication.api.transfer;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferDto;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferDtoMapper;
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/transfers")
@RequiredArgsConstructor
class TransferController {

    private final TransferFacade transferFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransfer(@Valid @RequestBody TransferRequest transferRequest) {
        TransferDto transferDto = TransferDtoMapper.transferDto(transferRequest);
        transferFacade.create(transferDto);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping(path = "pending")
    public void pullOutstandingTransfers() {
        transferFacade.payOutstandingTransfers();
    }
}
