package gitlab.AdrJ.bankapplication.Bankapplication.api.account;

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.AccountFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
class AccountController {

    private final AccountFacade accountFacade;

    @PostMapping(path = "users/{userId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void accountCreate(@Min(1) @PathVariable long userId) {
        accountFacade.createAccount(userId);
    }

}
