package gitlab.AdrJ.bankapplication.Bankapplication

import gitlab.AdrJ.bankapplication.Bankapplication.api.account.AccountController
import gitlab.AdrJ.bankapplication.Bankapplication.api.transfer.TransferController
import gitlab.AdrJ.bankapplication.Bankapplication.api.user.UserController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = BankApplication.class)
class LoadContextTest {

    @Autowired(required = false)
    private UserController userController

    @Autowired(required = false)
    private TransferController transferController

    @Autowired(required = false)
    private AccountController accountController

    def "Should create all beans"() {
        expect:
        userController
        transferController
        accountController
    }
}
