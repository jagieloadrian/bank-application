package gitlab.AdrJ.bankapplication.Bankapplication.infrastructure.user

import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.UpdateUserClient
import gitlab.AdrJ.bankapplication.Bankapplication.domain.User.User
import spock.lang.Specification

class UpdateUserPostgresClientTest extends Specification {

    UserRepository userRepository = Mock(UserRepository)
    UpdateUserClient updateUserClient = new UpdateUserPostgresClient(userRepository)
    User user = User.builder()
            .login("ValidLogin")
            .build();

    def "Update user login correctly"() {
        given:
        userRepository.getOne(1) >> user
        when:
        updateUserClient.changeLogin(1, "Kamilek")
        then:
        user.getLogin() == "Kamilek"
    }

}
