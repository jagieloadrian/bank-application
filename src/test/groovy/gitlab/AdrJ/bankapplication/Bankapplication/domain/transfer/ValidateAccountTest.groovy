package gitlab.AdrJ.bankapplication.Bankapplication.domain.transfer

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.Account
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.TransferException
import gitlab.AdrJ.bankapplication.Bankapplication.domain.Transfer.ValidateAccount
import spock.lang.Specification

class ValidateAccountTest extends Specification {

    Account accountFrom = Account.builder().amount(BigDecimal.valueOf(200)).build()
    BigDecimal amount = BigDecimal.valueOf(1210)

    ValidateAccount validateAccount = new ValidateAccount();

    def "Should throw exception if not enough money"() {
        when:
        validateAccount.validateAmount(accountFrom, amount)
        then:
        thrown TransferException.class
    }
}
