package gitlab.AdrJ.bankapplication.Bankapplication.domain.account

import gitlab.AdrJ.bankapplication.Bankapplication.domain.Account.Account
import spock.lang.Specification

class AccountTest extends Specification {

    Account account = Account.builder().amount(BigDecimal.valueOf(1000)).build()

    def "Should subtract amount correctly"() {
        given:
        account.subtractAmount(a)
        expect:
        account.getAmount() == BigDecimal.valueOf(b)
        where:

        a      | b
        1000   | 0
        500    | 500
        30     | 970
        349.99 | 650.01
    }

    def "Should add to amount correctly"() {
        given:
        account.addAmount(a)
        expect:
        account.getAmount() == BigDecimal.valueOf(b)
        where:
        a      | b
        1000   | 2000
        500    | 1500
        30     | 1030
        349.99 | 1349.99
    }
}
